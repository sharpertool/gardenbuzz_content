# Introduction

There are instructions on how to setup the nursery information folder

First, name the folder with the nursery id, a space, a dash, a space, and then whaterver name you want.

Exampe:
<pre>
1 - Edwards
2 - GEM
3 - Blah
</pre>

In the folder, you need to place photos, videos, and any other information yhou have collected from the nursery. You will also need to create files that describe this information. The format of the files must be something I can read from a Python script, so I'm going to give you some specific formatting guidlines that you need to follow pretty much exactly.

The files to add are:

- about.txt
- photos.txt

This list might change.. it's a work in progress!

# About.txt

This is just a file that includes the 'about' text for the nursery. You can make this as long or short as you like. If the file does not exist, I'll assume there is nothing to say.

If you want to do some formatting, you can use html formatting. I'll have to work with you on the details, and I'd like to use 'span' tags and classes to identify things.

If you want to format something a differnet way, such as bold, highlight it, then lets work out some details.

## To Bold Something
<span class='gbbold'>The text to bold</span>

## About Example

Edwards Nursery is a nursery in the north end, It has many acres of cool stuff, lots of <span class='gbbold'>perenials</span> and annuals.

# Photos.ini

This file will describe the photos you have placed into the folder.

First, I need to know the 'main' photo for the site. This is the header photo, and the thumbnail for the nursery list. Use the keyword 'main' followed by a semicolon to identify this photo. Use the keyword main_t to define the thumbnail version of this photo

<pre>
[main]
img: edwards1.png
thumb: edwards1_thumb.png
caption: blah blah
</pre>

## Other photots

For all other photos, use a block of information for the photo

<pre>
[photo1]
img: filename
thumb: thumbnail file
caption: caption for this file
other_keys_for_later: info

[photo2]
img: filename
thumb: thumbnail file
caption: caption for this file
other_keys_for_later: info
</pre>

