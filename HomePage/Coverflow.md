# CoverFlow

## Example

![Front Garden](../Images/HomePage/TheImage.jpg "Image title")

This is a paragraph or two of text

## Rose pruning

![Mr. Lincoln Small](../Images/HomePage/Height of Mr Lincoln.jpg "See how tall Mr. Lincoln can get.")

Mister Lincoln, a hybrid tea, in early April before pruning.  This rose will be pruned to a shorter height.

## Vicki's Front Garden

![Front Garden](../Images/HomePage/FrontGarden.jpg "Front of my house")

This is the front garden at my house.

## Lilacs

![Front Garden](../Images/HomePage/FrontGarden.jpg "Front of my house")

A paragraph about lilacs

## Rose Borer

![](../Images/HomePage/rose borer.jpg "Rose border!!")

Yeah, we don't like these! Nasty and pernicious.

# Front Page

These are front page items - I will use a 2nd level heading for each, with any text following that in a span. The layout will have to be figured out... but will basically be fixed into known 'locations'

## We love gardening in the Treasure Valley

GardenBuzz wants to show you the best things about gardening here in the Treasure Valley.  Take a tour of our Featured Gardeners and see what beautiful abundance they have created.  They will show you how to make the best of our changeable weather, low humidity and challenging soils.

## Check out our Featured Gardeners

Peek inside some of the most beautiful gardens in the Treasure Valley.  Learn new ways to make your garden beautiful, easy to maintain.
Survey different garden techniques for growing food that to fill up the dinner table!

## Garden Coach can help you garden

Hands-on Garden instruction for the beginner gardener. We come to your garden and work with you.

