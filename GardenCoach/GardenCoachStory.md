Garden Coach Story
===Several years ago, after moving from the west coast to BoiseEntered a large nursery to buy plants for our new homeOne acre of land to fill with purple pink red flowers, red blooming roses on 7 foot arches welcoming all who would come into my garden Large luscious tomatoes, the table groaning with beautiful peppers, yellow squash, luscious strawberries, blackberries, blueberries. I knew gardening was different here, soil different sun hot and intenseWater a challengeWalked into the nursery spent 2 hours there roaming around and reading plant tags.  Went home, spent 0$. Cried in the car. Why, too much stuff, too many plants, too many choices, afraid I would pick out the wrong colors, not heat tolerant, In short, I talked myself out of buying plants. It was scary. 



